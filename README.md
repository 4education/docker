# Dockerezation

# Require

- docker (https://docs.docker.com/install/linux/docker-ce/ubuntu/) - Installation guide

# Pre requirements

Dockerfile uses for build container only. If you need specify
libraris or tools in some container add it to Dockerfile
and run rebuild

Before launch containers you should have 4 repository with folder name that pointed below:
* repo - https://gitlab.com/surveyProjectGroup/result_service; folder name - result_service
* repo - https://gitlab.com/surveyProjectGroup/launch_service; folder name - launch_service
* repo - https://gitlab.com/surveyProjectGroup/configuration_service; folder name - configuration_service
* repo - https://gitlab.com/surveyProjectGroup/frontend; folder name - configuration_front

Make sure that you clone all repositories and named them like i described above. Than launch.

* Create *.redefine* file for redefine variables if you need it. *DO NOT CHANGE .env FILE*

# Launch
```
docker-compose build ( First time or when changed configuration of container )
docker-compose up ( Use *-d* for launch without console output )
```

# Down containers
```
docker-compose down
